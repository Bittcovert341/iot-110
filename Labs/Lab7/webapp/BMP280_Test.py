# =============================================================================
# main to test from CLI
def main():

    # create an instance of my pi bmp280 sensor object
    pi_bmp280 = PiBMP280()

    # Read the Sensor ID.
    (chip_id, chip_version) = pi_bmp280.readBMP280ID()
    print "    Chip ID :", chip_id
    print "    Version :", chip_version

    # Read the Sensor Temp/Pressure values.
    (temperature, pressure) = pi_bmp280.readBMP280All()
    print "Temperature :", temperature, "C"
    print "   Pressure :", pressure, "hPa"

if __name__=="__main__":
   main()
# =============================================================================
